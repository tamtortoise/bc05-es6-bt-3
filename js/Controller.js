function renderTodoList(todos) {
  var contentHTML = "";
  todos.forEach(function (item) {
    contentHTML += `
    <li> ${item.desc} <i class="fa fa-trash-alt my-auto ml-auto" onclick="removeTodo(${item.id})"></i> <input id="statusToDo" type="checkbox" ${
      item.isComplete ? "checked" : ""
    } onclick="changeStatus()"/>    </li>`;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};

// Thêm hàm lấy thông tin từ Form cho những giá trị thường xuyên lặp lại
function layThongTinTuForm() {
  var desc = document.getElementById("newTask").value;

  return {
    desc: desc,
  };
};
